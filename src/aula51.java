import java.util.Arrays; // atividade com arrays II
import java.util.Scanner;

public class aula51 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int[] valores = new int [7];

        for(int i = 0; i<7; i++){
            System.out.printf("Digite o numero da posição [%d] ", (i+1));
            valores[i] = in.nextInt();
            // 0, 1, 2, 3, 4, 5, 6,
        }

        int somaElementos = 0;
        for (int x = 0; x<7; x++){
            somaElementos +=valores[x];
            somaElementos = somaElementos + valores[x];
        }
        System.out.printf("A soma dos elementos é: %d\n", somaElementos);

        for(int x = 0; x<7; x++){
            int produto = (valores[x] * x);
            System.out.printf("A multiplicação do indice pelo elemento é: %d * %d = d%\n", produto);
        }

        System.out.println(Arrays.toString(valores));

    }
}
